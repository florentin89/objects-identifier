//
//  ViewController.swift
//  ObjectsIdentifier
//
//  Created by Florentin on 23/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ObjectsIdentifierViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var selectedImageView: UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            selectedImageView.image = selectedImage
            
            guard let ciImage = CIImage(image: selectedImage) else {fatalError("Could not convert UIImage to CIImage")}
            
            detect(image: ciImage)
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func detect(image: CIImage){
        
        guard let model = try? VNCoreMLModel(for: Inceptionv3().model) else {fatalError("Loading CcoreML Model failed.")}
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
            
            guard let results = request.results as? [VNClassificationObservation] else {fatalError("Model failed to process image.")}
            
            if let firstResult = results.first {
                
                self.navigationItem.title = firstResult.identifier
                
//                if firstResult.identifier.contains("hotdog"){
//                    self.navigationItem.title = "Hotdog !"
//                } else {
//                    self.navigationItem.title = "Not Hotdog !"
//                }
            }
        }
        
        let handler = VNImageRequestHandler(ciImage: image)
        
        do{
            try handler.perform([request])
        } catch{
            print("Error, \(error)")
        }
        
    }
    
    @IBAction func cameraTapped(_ sender: UIBarButtonItem) {
        
        present(imagePicker, animated: true, completion: nil)
    }
}
